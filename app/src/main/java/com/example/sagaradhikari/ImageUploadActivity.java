package com.example.sagaradhikari;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;

import android.os.Bundle;

import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.Locale;


public class ImageUploadActivity extends AppCompatActivity {
    private static final long START_TIME_IN_MILLIS = 6000;
    private CircularImageView imageView;
    private Button clickBtn,submitBtn,logoutBtn;
    private static final int REQUEST_CODE=101;

    private TextView mTextViewCountDown;
    private CountDownTimer mCountDownTimer;

    private boolean mTimerRunning;

    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);

        initialization();
    }

    private void initialization() {
        imageView = findViewById(R.id.circle_image);
        logoutBtn=findViewById(R.id.logout);
        mTextViewCountDown = findViewById(R.id.text_view_countdown);
        clickBtn = findViewById(R.id.click_me);
        submitBtn=findViewById(R.id.submit_me);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
                clickBtn.setVisibility(View.GONE);
                submitBtn.setVisibility(View.VISIBLE);
            }
        });
            clickBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCamera();
                    clickBtn.setVisibility(View.GONE);
                    submitBtn.setVisibility(View.VISIBLE);
                }
            });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ImageUploadActivity.this,CurrentLocation.class);
                startActivity(intent);
            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager preferencesManager = new PreferenceManager(ImageUploadActivity.this);
                preferencesManager.logout();
                startTimer();

            }
        });

    }


    private void openCamera(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]
                    {Manifest.permission.CAMERA},REQUEST_CODE);

            return;
        }

        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,0);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap bitmap= (Bitmap) data.getExtras().get("data");
        imageView.setImageBitmap(bitmap);
    }

    private void startTimer() {
        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                Intent logoutIntent = new Intent(ImageUploadActivity.this, SignupActivity.class);
                startActivity(logoutIntent);
                finishAffinity();
                Toast.makeText(ImageUploadActivity.this, "Logout", Toast.LENGTH_SHORT).show();
            }
        }.start();

        mTimerRunning = true;

    }

    private void updateCountDownText() {
        int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        mTextViewCountDown.setText("Logging out in "+timeLeftFormatted);
    }
}