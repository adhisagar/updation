package com.example.sagaradhikari;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    Context context;
    public PreferenceManager(Context context){
        this.context=context;
    }

    public void saveSignupDetails(String email,String password){
        SharedPreferences sharedPreferences=context.getSharedPreferences("Preference",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("email",email);
        editor.putString("password",password);
        editor.apply();
        editor.commit();
    }

    public void saveLoginDetails(String email,String password){
        SharedPreferences sharedPreferences=context.getSharedPreferences("Preference",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("email",password);
        editor.putString("password",password);
        editor.apply();
        editor.commit();
    }
    public String getPassword(){
        SharedPreferences sharedPreferences=context.getSharedPreferences("Preference",Context.MODE_PRIVATE);
        return sharedPreferences.getString("password","");
    }

    public void logout(){
        SharedPreferences sharedPreferences=context.getSharedPreferences("Preference",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

}