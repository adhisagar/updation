package com.example.sagaradhikari;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME="user.db";
    public static final String TABLE_NAME="Suposhamtest";
    public static final String USER_NAME="name";
    public static final String USER_EMAIL="email";
    public static final String USER_PHONE="phone";

    String createTable="CREATE TABLE \"user\" (\n" +
            "\t\"id\"\tINTEGER,\n" +
            "\t\"name\"\tTEXT,\n" +
            "\t\"email\"\tTEXT,\n" +
            "\t\"number\"\tTEXT\n" +
            ")";

    public DataBaseHelper(Context context){
        super(context,DATABASE_NAME,null,1);
        getWritableDatabase().execSQL(createTable);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+TABLE_NAME +"(name Text,email Text,phone int)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
    }

    public boolean insertData(String userName,String email,String phone){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(USER_NAME,userName);
        contentValues.put(USER_EMAIL,email);
        contentValues.put(USER_PHONE,phone);

        long result=db.insert(TABLE_NAME,null,contentValues);
        db.close();

        if (result==-1){
            return false;
        } else {
            return true;
        }
    }

    public Cursor getAllData(){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res=db.rawQuery("Select * from " +TABLE_NAME,null);
        return res;
    }
}
