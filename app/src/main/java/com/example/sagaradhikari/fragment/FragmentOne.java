package com.example.sagaradhikari.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.sagaradhikari.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentOne extends Fragment {


    public FragmentOne() {
        // Required empty public constructor
    }
    private FragmentOneListener listener;
    private TextView textView1;
    private Button bgButton;

    public interface FragmentOneListener{
        void inputASend(CharSequence input, int image);
       // void changeBg(int input1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragment_one, container, false);
        textView1=view.findViewById(R.id.frag_one_text);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence input = textView1.getText();
                //listener.inputASend(input,R.color.btnYellow);
               // listener.changeBg(R.color.btnYellow);
            }
        });
        bgButton=view.findViewById(R.id.bgButton);

        bgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }

    public void updateEditText(CharSequence newText) {
        textView1.setText(newText);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentOneListener){
            listener= (FragmentOneListener) context;
        } else {
            throw new RuntimeException(context.toString()+"must implement fragone listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener=null;
    }
}
