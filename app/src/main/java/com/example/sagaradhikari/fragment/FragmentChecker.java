package com.example.sagaradhikari.fragment;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.sagaradhikari.R;

public class FragmentChecker extends AppCompatActivity implements FragmentOne.FragmentOneListener,FragmentTwo.FragmentTwoListener{

    private FragmentOne fragmentOne;
    private FragmentTwo fragmentTwo;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_checker);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle("Cargo Deal");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        fragmentOne = new FragmentOne();
        fragmentTwo = new FragmentTwo();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frag_one, fragmentOne, null)
                .replace(R.id.frag_two, fragmentTwo, null)
                .commit();
    }


    @Override
    public void inputASend(CharSequence input,int image) {
        fragmentTwo.updateEditText(input,image);
        getSupportActionBar().setBackgroundDrawable(getDrawable(image));
    }

//    @Override
//    public void changeBg(int input1) {
//        fragmentTwo.changeBackground(input1);
//        getSupportActionBar().setBackgroundDrawable(getDrawable(input1));
//    }

    @Override
    public void inputBSend(CharSequence input) {
        fragmentOne.updateEditText(input);
    }
}
