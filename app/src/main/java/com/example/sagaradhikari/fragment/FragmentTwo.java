package com.example.sagaradhikari.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.sagaradhikari.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTwo extends Fragment {


    public FragmentTwo() {
        // Required empty public constructor
    }

    FragmentTwoListener listener;
    private TextView textView2;

    public interface FragmentTwoListener{
        void inputBSend(CharSequence input);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragment_two, container, false);
        textView2=view.findViewById(R.id.frag_two_text);

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence input = textView2.getText();
                listener.inputBSend(input);
            }
        });
        return view;
    }
    public void updateEditText(CharSequence newText,int image) {
        textView2.setText(newText);
        textView2.setBackgroundColor(image);
    }

//    public void changeBackground(int text){
//        textView2.setBackgroundColor(text);
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentTwoListener){
            listener= (FragmentTwoListener) context;
        } else {
            throw new RuntimeException(context.toString()+"implements frag two listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener=null;
    }
}
