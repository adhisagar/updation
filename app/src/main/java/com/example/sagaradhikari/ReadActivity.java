package com.example.sagaradhikari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ReadActivity extends AppCompatActivity {
    DataBaseHelper myDb;
    TextView userDetails;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        initilize();
    }

    private void initilize() {
        myDb=new DataBaseHelper(this);
        userDetails=findViewById(R.id.user_details_record);

        getData();
    }

    private void getData(){
        Cursor res=myDb.getAllData();
        StringBuffer stringBuffer=new StringBuffer();
        if (res !=null && res.getCount()>0){
            while (res.moveToNext()){
                stringBuffer.append("UserName: "+res.getString(0)+" UserEmail: "+res.getString(1)+" Phone: "+res.getString(2)+"\n");
            }
            userDetails.setText(stringBuffer.toString());
            Toast.makeText(this, "Data get Successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "There is no data", Toast.LENGTH_SHORT).show();
        }
    }
}
