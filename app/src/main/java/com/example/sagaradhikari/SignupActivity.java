package com.example.sagaradhikari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.HashMap;
import java.util.Map;

public class SignupActivity extends AppCompatActivity {

    //private Toolbar toolbar;
    private EditText signup_email, signup_password;
    private Button signupBtn;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        initialize();
        setEmail();
    }

    private void initialize() {

//        toolbar=findViewById(R.id.toolbar);
//        toolbar.setTitle("SignUp");
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        signup_email = findViewById(R.id.singnup_email);
        signup_password = findViewById(R.id.signup_password);

        signupBtn = findViewById(R.id.signup_btn);
        signupBtn.setEnabled(false);
        signupBtn.setTextColor(Color.argb(50, 255, 255, 255));
    }


    private void setEmail() {

        signupBtn.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkInputs();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        signup_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkInputs();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkEmailAndPassword();

            }
        });

    }


    private void checkInputs() {
        if (!TextUtils.isEmpty(signup_email.getText())) {
            signupBtn.setEnabled(true);
            signupBtn.setTextColor(Color.rgb(255, 255, 255));
            if (!TextUtils.isEmpty(signup_password.getText()) && signup_password.length() >= 8) {
                signupBtn.setEnabled(true);
                signupBtn.setTextColor(Color.rgb(255, 255, 255));
            } else {
                signupBtn.setEnabled(false);
                signupBtn.setTextColor(Color.argb(50, 255, 255, 255));
            }

        } else {
            signupBtn.setEnabled(false);
            signupBtn.setTextColor(Color.argb(50, 255, 255, 255));
        }

    }


    private void checkEmailAndPassword() {
        Drawable customErrorIcon = getResources().getDrawable(R.drawable.error_small);
        customErrorIcon.setBounds(0, 0, customErrorIcon.getIntrinsicWidth(), customErrorIcon.getIntrinsicHeight());


        if (signup_email.getText().toString().matches(emailPattern)) {

            signupBtn.setEnabled(false);
            signupBtn.setTextColor(Color.argb(50, 255, 255, 255));
            final String email = signup_email.getText().toString();
            final String password = signup_password.getText().toString();
            new PreferenceManager(SignupActivity.this).saveSignupDetails(email, password);

            Intent intent = new Intent(SignupActivity.this, MainActivity.class);
            intent.putExtra("email", email);
            startActivity(intent);


        } else {
            signupBtn.setEnabled(true);
            signupBtn.setTextColor(Color.rgb(255, 255, 255));
            Toast.makeText(SignupActivity.this, "Not successful", Toast.LENGTH_SHORT).show();
        }

    }
}
