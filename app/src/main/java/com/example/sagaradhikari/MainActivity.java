package com.example.sagaradhikari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private EditText name,phone_no;
    String signupuEmail;
    private TextView email;
    private Button submitBtn,readBtn;

    DataBaseHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mydb=new DataBaseHelper(this);
        initialize();
    }

    private void initialize() {
        name=findViewById(R.id.name);
        phone_no=findViewById(R.id.phone_no);

        signupuEmail=getIntent().getStringExtra("email").toString();


        email=findViewById(R.id.email);
        email.setText(signupuEmail);

        readBtn=findViewById(R.id.db_readBtn);
        readBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ReadActivity.class);
                startActivity(intent);
            }
        });
        submitBtn=findViewById(R.id.db_submitBtn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitDetails();
            }
        });
    }

    private void submitDetails() {
        String username,phone;
        username=name.getText().toString();
        phone=phone_no.getText().toString();
        if (TextUtils.isEmpty(username)){
            name.setError("Enter your username");
            name.requestFocus();
        }

        else if (TextUtils.isEmpty(phone)){
            phone_no.setError("Enter correct number");
            phone_no.requestFocus();
        } else if (phone.length()!=10){
            phone_no.setError("Enter correct number");
            phone_no.requestFocus();
        }
     else {
            Boolean result = mydb.insertData(username, signupuEmail, phone);

            if (result == true) {
                Toast.makeText(this, "Data Inserted Successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, ImageUploadActivity.class);
                startActivity(intent);

            } else {
                Toast.makeText(this, "Unsuccessful", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
